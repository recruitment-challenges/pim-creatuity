<?php

declare(strict_types=1);

namespace App\Task\Product;

interface ProductInterface
{
    /**
     * @param string $name Product name
     */
    public function setName(string $name): self;

    /**
     * @return string Product name
     */
    public function getName(): string;

    /**
     * @param string $sku Stock keeping unit
     */
    public function setSku(string $sku): self;

    /**
     * @return string Stock keeping unit
     */
    public function getSku(): string;

    /**
     * @param int $price Product's price defined in base monetary unit
     */
    public function setPrice(int $price): self;

    /**
     * @return int Product's price defined in base monetary unit
     */
    public function getPrice(): int;

    /**
     * Creates instance of a product from provided JSON string.
     *
     * @param string $productDefinition Valid product definition as a JSON string
     */
    public static function createFromJson(string $productDefinition): self;
}
