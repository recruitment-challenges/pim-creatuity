<?php

declare(strict_types=1);

namespace App\Task\Product;

class ProductFactory
{
    /**
     * @var array<ProductInterface> $products
     */
    private array $products = [];

    /**
     * @param ProductInterface $product
     */
    public function save(ProductInterface $product): bool
    {
        $this->products[] = $product;

        return true;
    }

    /**
     * @return array<ProductInterface> All products present in the persistance storage
     */
    public function readAll()
    {
        return $this->products;
    }
}
