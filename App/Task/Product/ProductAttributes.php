<?php

declare(strict_types=1);

namespace App\Task\Product;

class ProductAttributes implements ProductAttributesInterface
{
    /**
     * @var array $attributes Key-value pairs where key is attribute name and value is attribute's value
     */
    private array $attributes = [];

    public function count(): int
    {
        return count($this->attributes);
    }

    public function add(string $attributeName, $attributeValue): self
    {
        $this->attributes[$attributeName] = $attributeValue;

        return $this;
    }

    public function read(string $attributeName)
    {
        if (!array_key_exists($attributeName, $this->attributes)) {
            throw new \OutOfBoundsException('Attribute does not exists');
        }

        return $this->attributes[$attributeName];
    }
}
