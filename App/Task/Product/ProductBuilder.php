<?php

declare(strict_types=1);

namespace App\Task\Product;

class ProductBuilder
{
    /**
     * Builds products of proper type based on provided product's JSON definition.
     *
     * @param string $jsonProductDefinition
     */
    public static function buildProductFromJson(string $jsonProductDefinition): ProductInterface
    {
        return StandardProduct::createFromJson($jsonProductDefinition);
    }
}
