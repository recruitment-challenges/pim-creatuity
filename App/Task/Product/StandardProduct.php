<?php

declare(strict_types=1);

namespace App\Task\Product;

use stdClass;

class StandardProduct implements ProductInterface
{
    /**
     * @var string visible for client name of the product
     */
    private string $name;

    /**
     * @var string stock keeping unit @link: https://en.wikipedia.org/wiki/Stock_keeping_unit
     */
    private string $sku;

    /**
     * @var int product price defined in basic monetary unit (e.g. 1 cent for USD, 1 grosz for PLN)
     */
    private int $price;

    /**
     * @var ProductAttributes
     */
    private ProductAttributes $attributes;

    public function __construct()
    {
        $this->attributes = new ProductAttributes();
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @inheritdoc
     *
     * @throws OutOfBoundsException if provided price is lower then 0
     */
    public function setPrice(int $price): self
    {
        if (0 > $price) {
            throw new \OutOfBoundsException('Price can not be lower then 0');
        }

        $this->price = $price;

        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getAttributes(): ProductAttributes
    {
        return $this->attributes;
    }

    public static function createFromJson(string $productDefinition): self
    {
        $json = json_decode($productDefinition);

        self::validateProductDefinition($json);

        $product = (new self())
            ->setName($json->name)
            ->setSku($json->sku)
            ->setPrice($json->price)
        ;

        if (isset($json->attributes)) {
            self::addAttributesFromJson($product, $json->attributes);
        }

        return $product;
    }

    /**
     * @param self            $product
     * @param array<stdClass> $attributes Product's attributes
     */
    private static function addAttributesFromJson(self $product, array $attributes): void
    {
        foreach ($attributes as $attribute) {
            $product->getAttributes()->add($attribute->name, $attribute->value);
        }
    }

    /**
     * Validates if provided JSON object is a correct product definition.
     *
     * @param stdClass $product Decoded JSON with product definition
     *
     * @throws DomainException if provided JSON is not correctly defined product
     */
    private static function validateProductDefinition(?stdClass $product): void
    {
        if (null === $product) {
            throw new \DomainException('Invalid JSON');
        }

        $invalidDefinition = (
            !isset($product->name)
            || !isset($product->sku)
            || !isset($product->price)
        );

        if ($invalidDefinition) {
            throw new \DomainException('Invalid product definition. Name, SKU and price must exists');
        }

        if (!isset($product->attributes) || empty($product->attributes)) {
            return;
        }

        foreach ($product->attributes as $attribute) {
            $invalidAttribute = (
                !isset($attribute->name)
                || !isset($attribute->value)
                || !isset($attribute->type)
            );

            if ($invalidAttribute) {
                throw new \DomainException('Invalid attribute definition. Name, value and type must exists');
            }
        }
    }
}
