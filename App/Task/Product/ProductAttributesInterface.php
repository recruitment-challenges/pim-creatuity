<?php

declare(strict_types=1);

namespace App\Task\Product;

interface ProductAttributesInterface
{
    /**
     * @return int quantity of attributes associated with product
     */
    public function count(): int;

    /**
     * @param string $attributeName
     * @param mixed  $attributeValue
     */
    public function add(string $attributeName, $attributeValue): self;

    /**
     * @param string $attributeName
     *
     * @return mixed Attribute's value
     */
    public function read(string $attributeName);
}
