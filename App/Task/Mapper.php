<?php

declare(strict_types=1);

namespace App\Task;

use App\Task\Product\ProductInterface;
use App\Task\Product\ProductFactory;
use App\Task\Product\ProductBuilder;

/**
* @category Creatuity
* @package Task
* @copyright Copyright (c) 2008-2020 Creatuity Corp. (http://www.creatuity.com)
* @license http://www.creatuity.com/license
*/
class Mapper
{

    /**
    * @var ProductFactory
    */
    private $productFactory;

    public function __construct(ProductFactory $productFactory)
    {
        $this->productFactory = $productFactory;
    }

    /**
     * @param string $productJson Valid JSON with product definition
     */
    public function mapProduct(string $productJson): ProductInterface
    {
        return ProductBuilder::buildProductFromJson($productJson);
    }
}
