# Magento Test

Recruitment challenge from Creatuity for Magento Backend Developer.

## How to start
```bash
# Go to directory where you whant this project to be stored:
cd path/to/project/storage/location

# Clone the Magento Test repository:
git clone git@gitlab.com:recruitment-challenges/magento-test.git .

# Install required packages:
composer install

# Verify if everything is ok by executing tests:
./vendor/bin/kahlan

# Generate documentation
./vendor/bin/phpdoc
```

## Solution description
1. Provided on entry template file to start with fits to Mapper design pattern so regardless of names this pattern is implemented. So ProductFactory serves as storage adapter and shell be used as a glue code for actual implementation of storage.
2. New product types can extend from StandardProduct type.
3. ProductBuilder is a place for the logic behind deciding about product type.
4. Product attributes currently are developed as associative array but any time it can be converted to collection of Attribute Objects with name, type and value properties. Also property builder can be implemented to decide about attribute type.
