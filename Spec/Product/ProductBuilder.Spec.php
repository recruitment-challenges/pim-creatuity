<?php

declare(strict_types=1);

namespace Tests\Product;

use App\Task\Product\ProductBuilder;
use App\Task\Product\ProductInterface;
use App\Task\Product\StandardProduct;

describe(
    'Product Builder',
    function () {
        it(
            'can create instance of a product',
            function () {
                $product = ProductBuilder::buildProductFromJson('{"name":"Product 1","sku":"Product1","price":100}');

                expect($product)->toBeAnInstanceOf(ProductInterface::class);
                expect($product)->toBeAnInstanceOf(StandardProduct::class);
            }
        );
    }
);
