<?php

declare(strict_types=1);

namespace Tests\Product;

use App\Task\Product\ProductFactory;
use App\Task\Product\StandardProduct;

describe(
    'Product Factory',
    function () {
        it(
            'can be instantiated',
            function () {
                expect(new ProductFactory())->toBeAnInstanceOf(ProductFactory::class);
            }
        );

        describe(
            'operations',
            function () {
                given(
                    'pf',
                    function () {
                        return new ProductFactory();
                    }
                );

                it(
                    'can save product to the persistance storage',
                    function () {
                        $product = new StandardProduct();

                        expect($this->pf->save($product))->toBe(true);
                    }
                );

                it(
                    'can read all products from the persistance storage',
                    function () {
                        $this->pf->save(new StandardProduct());
                        $this->pf->save(new StandardProduct());
                        $this->pf->save(new StandardProduct());

                        expect(count($this->pf->readAll()))->toBe(3);
                    }
                );
            }
        );
    }
);
