<?php

declare(strict_types=1);

namespace Tests\Product;

use App\Task\Product\StandardProduct;
use App\Task\Product\ProductAttributes;
use App\Task\Product\ProductInterface;

describe(
    'Standard Product',
    function () {
        it(
            'can be instantiated',
            function () {
                expect(new StandardProduct())->toBeAnInstanceOf(StandardProduct::class);
                expect(new StandardProduct())->toBeAnInstanceOf(ProductInterface::class);
            }
        );

        given(
            'p',
            function () {
                return new StandardProduct();
            }
        );

        describe(
            'will have',
            function () {
                it(
                    'property "name" available to set',
                    function () {
                        expect($this->p->setName('Test product'))->toBeAnInstanceOf(ProductInterface::class);
                    }
                );

                it(
                    'property "name" available to read',
                    function () {
                        $this->p->setName('Test product');

                        expect($this->p->getName())->toBeA('string');
                        expect($this->p->getName())->toBe('Test product');
                    }
                );

                it(
                    'property "sku" available to set',
                    function () {
                        expect($this->p->setSku('ABC123'))->toBeAnInstanceOf(ProductInterface::class);
                    }
                );

                it(
                    'property "sku" available to read',
                    function () {
                        $this->p->setSku('ABC123');

                        expect($this->p->getSku())->toBeA('string');
                        expect($this->p->getSku())->toBe('ABC123');
                    }
                );

                it(
                    'property "price" available to set',
                    function () {
                        expect($this->p->setPrice(100))->toBeAnInstanceOf(ProductInterface::class);
                    }
                );

                it(
                    'property "price" available to read',
                    function () {
                        $this->p->setPrice(12300);

                        expect($this->p->getPrice())->toBeA('integer');
                        expect($this->p->getPrice())->toBe(12300);
                    }
                );
            }
        );

        describe(
            'attributes',
            function () {
                given(
                    'attr',
                    function () {
                        return $this->p->getAttributes();
                    }
                );

                it(
                    'are available to read',
                    function () {
                        expect($this->attr)->toBeAnInstanceOf(ProductAttributes::class);
                    }
                );

                it(
                    'there are no at start',
                    function () {
                        expect($this->attr->count())->toBe(0);
                    }
                );

                it(
                    'are available to set',
                    function () {
                        expect($this->attr->add('qty', 100))->toBeAnInstanceOf(ProductAttributes::class);
                        expect($this->attr->count())->toBe(1);
                    }
                );

                it(
                    'particular one is readable',
                    function () {
                        $this->attr->add('qty', 100);

                        expect($this->attr->read('qty'))->toBe(100);
                    }
                );

                it(
                    'reading not existing one throws error',
                    function () {
                        $readNotExistingAttribute = function () {
                            $this->attr->read('missing');
                        };

                        expect($readNotExistingAttribute)->toThrow(new \OutOfBoundsException('Attribute does not exists'));
                    }
                );
            }
        );

        describe(
            'will not allow',
            function () {
                it(
                    'to set price lower then 0',
                    function () {
                        $negativePriceSetUpResult = function () {
                            $this->p->setPrice(-10);
                        };

                        expect($negativePriceSetUpResult)->toThrow(new \OutOfBoundsException('Price can not be lower then 0'));
                    }
                );
            }
        );

        describe(
            'instantiation from state',
            function () {
                given(
                    'productsCorrectJsonDefinition',
                    function () {
                        return '{"name":"Product 1","sku":"Product1","price":100}';
                    }
                );

                given(
                    'productsCorrectJsonDefinitionWithAttributes',
                    function () {
                        return '{"name":"Product 1","sku":"Product1","price":100,"attributes":[{"type":"int","name":"qty","value":100}]}';
                    }
                );

                given(
                    'productsCorrectJsonDefinitionWithManyAttributes',
                    function () {
                        return '{"name":"Product 1","sku":"Product1","price":100,"attributes":[{"type":"int","name":"qty","value":100},{"type":"string","name":"description","value":"product description"}]}';
                    }
                );

                it(
                    'can be created from JSON string',
                    function () {
                        $product = StandardProduct::createFromJson($this->productsCorrectJsonDefinition);

                        expect($product)->toBeAnInstanceOf(ProductInterface::class);
                        expect($product->getName())->toBe('Product 1');
                        expect($product->getSku())->toBe('Product1');
                        expect($product->getPrice())->toBe(100);
                    }
                );

                it(
                    'can be created from JSON string with attributes',
                    function () {
                        $attr = StandardProduct::createFromJson($this->productsCorrectJsonDefinitionWithManyAttributes)
                            ->getAttributes()
                        ;

                        expect($attr)->toBeAnInstanceOf(ProductAttributes::class);
                        expect($attr->count())->toBe(2);
                        expect($attr->read('qty'))->toBeAn('integer');
                        expect($attr->read('qty'))->toBe(100);
                        expect($attr->read('description'))->toBeA('string');
                        expect($attr->read('description'))->toBe('product description');
                    }
                );

                it(
                    'can be created from JSON string with many attributes',
                    function () {
                        $attr = StandardProduct::createFromJson($this->productsCorrectJsonDefinitionWithAttributes)
                            ->getAttributes();

                        expect($attr)->toBeAnInstanceOf(ProductAttributes::class);
                        expect($attr->read('qty'))->toBeAn('integer');
                        expect($attr->read('qty'))->toBe(100);
                    }
                );

                it(
                    'can not be created from wrong JSON string',
                    function () {
                        $wrongJsonInstantiationResult = function () {
                            return StandardProduct::createFromJson('{"name2":"Product 1","sku2":"Product1","price2":100}');
                        };

                        expect($wrongJsonInstantiationResult)->toThrow(new \DomainException('Invalid product definition. Name, SKU and price must exists'));

                        $wrongJsonInstantiationResult = function () {
                            return StandardProduct::createFromJson('{"name":"Product 1,"sku":"Product1","price":100}');
                        };

                        expect($wrongJsonInstantiationResult)->toThrow(new \DomainException('Invalid JSON'));

                        $wrongJsonInstantiationResult = function () {
                            return StandardProduct::createFromJson('{"name":"Product 1","sku":"Product1","price":100,"attributes":[{"name":"qty"}]}');
                        };

                        expect($wrongJsonInstantiationResult)->toThrow(new \DomainException('Invalid attribute definition. Name, value and type must exists'));
                    }
                );
            }
        );
    }
);
