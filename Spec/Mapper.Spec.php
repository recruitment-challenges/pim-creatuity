<?php

declare(strict_types=1);

namespace Tests\Product;

use App\Task\Product\ProductFactory;
use App\Task\Product\ProductInterface;
use App\Task\Mapper;

describe(
    'Mapper',
    function () {
        given(
            'pf',
            function () {
                return new ProductFactory();
            }
        );

        it(
            'can be instantiated',
            function () {
                expect(new Mapper($this->pf))->toBeAnInstanceOf(Mapper::class);
            }
        );

        describe(
            'can create product instance',
            function () {
                given(
                    'mapper',
                    function () {
                        return new Mapper($this->pf);
                    }
                );

                it(
                    'from invalid JSON string',
                    function () {
                        $invalidJsonResult = function () {
                            $this->mapper->mapProduct('');
                        };

                        expect($invalidJsonResult)->toThrow(new \DomainException('Invalid JSON'));
                    }
                );

                it(
                    'from valid JSON string',
                    function () {
                        $productDefinition = '{"name":"Product 1","sku":"Product1","price":100,"attributes":[{"type":"int","name":"qty","value":100},{"type":"string","name":"description","value":"product description"}]}';
                        $product = $this->mapper->mapProduct($productDefinition);

                        expect($product)->toBeAnInstanceOf(ProductInterface::class);
                        expect($product->getName())->toBe("Product 1");
                        expect($product->getSku())->toBe("Product1");
                        expect($product->getPrice())->toBe(100);
                        expect($product->getAttributes()->count())->toBe(2);
                        expect($product->getAttributes()->read('qty'))->toBe(100);
                        expect($product->getAttributes()->read('description'))->toBe('product description');
                    }
                );
            }
        );
    }
);
